# Recognize Multiples 3 and 5 

Function that will print number, for multiples of three print “Three” instead 

of the number and for the multiples of five print “Five”.


### Virtual Environment

To create a virtual environment, simply type in the following command:

```sh
$ make create-env
```

### Run Test Scurri

Call the function and pass numbers from 1 to 100
```sh
$  make run-test-scurri
```

### Tests
For run test
```sh
$ make
```

In this project, there is a pipeline, on Gitlab, that executes the test 
convention and code to verify that the change is correct 
and follows the pattern.

If no pipeline is passed, a change will not be allowed to merge with the master