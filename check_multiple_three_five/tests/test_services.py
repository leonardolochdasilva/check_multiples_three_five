from check_multiple_three_five.services import check_multiples_tree_five


def test_should_output_threefive():
    # GIVEN
    number = 15
    result_expected = "ThreeFive"

    # WHEN
    result = check_multiples_tree_five(number)

    # THEN
    assert result == result_expected


def test_should_output_three():
    # GIVEN
    number = 18
    result_expected = "Three"

    # WHEN
    result = check_multiples_tree_five(number)

    # THEN
    assert result == result_expected


def test_should_output_five():
    # GIVEN
    number = 25
    result_expected = "Five"

    # WHEN
    result = check_multiples_tree_five(number)

    # THEN
    assert result == result_expected


def test_should_output_number():
    # GIVEN
    number = 22
    result_expected = number

    # WHEN
    result = check_multiples_tree_five(number)

    # THEN
    assert result == result_expected
