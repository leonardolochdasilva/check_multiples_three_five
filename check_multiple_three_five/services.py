def check_multiples_tree_five(number):
    is_multiple_tree = (number % 3) == 0
    is_multiple_five = (number % 5) == 0

    result_three = "Three" if is_multiple_tree else ""
    result_five = "Five" if is_multiple_five else ""
    results = f'{result_three}{result_five}'

    return results if results else number
